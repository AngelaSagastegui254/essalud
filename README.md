# Asistente Virtual

Asistente Virtual is a backend application ....

## Installation

### Requirements

To run this project locally, you must to have:

- [NodeJS 12.13](https://nodejs.org/es/download/package-manager/)
- [MySql](https://dev.mysql.com/downloads/)

### Configuring

1. Copy .env file and request or complete credentials

```bash
$ cp .env.example .env
```

2. Install NodeJs dependencies

```bash
$ npm install
```

### Usage

To start the app, run in your terminal

```bash
$ npm run dev
```
