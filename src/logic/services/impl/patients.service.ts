import { PatientsRepository } from '@data/repositories';
import { injectable } from 'inversify';
import { getCustomRepository } from 'typeorm';
import { IService } from '../interfaces/IService.interface';
import { GetPatientDto } from '@logic/dtos';

@injectable()
export class PatientsService implements IService<GetPatientDto> {
  async all(): Promise<[GetPatientDto[], number]> {
    try {
      const patientsRepository = getCustomRepository(PatientsRepository);
      const patients = await patientsRepository
        .createQueryBuilder('p')
        .getManyAndCount();

      return GetPatientDto.fromMany(patients[0], patients[1]);
    } catch (error) {
      return Promise.reject(error);
    }
  }
  async findOne(id: number): Promise<GetPatientDto | undefined> {
    try {
      const patientsRepository = getCustomRepository(PatientsRepository);
      const patient = await patientsRepository.findOne({ id });

      if (patient) {
        return GetPatientDto.from(patient);
      }
      return undefined;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
