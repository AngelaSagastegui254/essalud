import { Patient } from '@data/entities';

export class GetPatientDto {
  constructor(
    readonly id: number,
    readonly conadisCode: string,
    readonly code: string,
    readonly password: string
  ) {}

  static from(entity: Patient) {
    return new GetPatientDto(
      entity.id,
      entity.conadisCode,
      entity.code,
      entity.password
    );
  }

  static fromMany(
    patients: Patient[],
    count: number
  ): [GetPatientDto[], number] {
    const docs: GetPatientDto[] = patients.map((patient) =>
      GetPatientDto.from(patient)
    );
    return [docs, count];
  }
}
