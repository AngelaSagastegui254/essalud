import { EntityRepository, Repository } from 'typeorm';
import { Patient } from '../entities';

@EntityRepository(Patient)
export class PatientsRepository extends Repository<Patient> {}
