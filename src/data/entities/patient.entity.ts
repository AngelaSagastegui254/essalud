import { PrimaryGeneratedColumn, Entity, Column } from 'typeorm';

@Entity({
  name: 'patients',
})
export class Patient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 64,
    name: 'conadis_code',
  })
  conadisCode: string;

  @Column({
    length: 64,
  })
  code: string;

  @Column({
    length: 64,
  })
  password: string;
}
